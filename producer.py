import json
from time import sleep
from kafka import KafkaAdminClient, KafkaConsumer, KafkaProducer
from kafka.admin import NewTopic

BOOTSTRAP_SERVERS = ['localhost:9092']

admin_client = KafkaAdminClient(
    bootstrap_servers=BOOTSTRAP_SERVERS,
    client_id='admin'
)

# create new topic if not exists
consumer = KafkaConsumer(bootstrap_servers=BOOTSTRAP_SERVERS)
if 'app_topic' not in consumer.topics():
    topic_list = [
        NewTopic(
            name="app_topic",
            num_partitions=1,
            replication_factor=1
        )
    ]
    admin_client.create_topics(new_topics=topic_list, validate_only=False)

producer = KafkaProducer(
    bootstrap_servers=BOOTSTRAP_SERVERS,
    value_serializer=lambda x: json.dumps(x).encode('utf-8')
)

for e in range(10):
    data = {'number': e}
    producer.send('app_topic', value=data)
    print('send: ' + str(data))
    sleep(1)

admin_client.delete_topics(['app_topic'])
