import os
import subprocess
import pytest
from dotenv import load_dotenv
from unittest import mock
from urllib.parse import urlparse
from attrdict import AttrDict

from django.conf import settings
from django.test.client import RequestFactory
from rest_framework.test import APIClient

from .views import heartbeat
from consumer import import_env, request, get_process
import consumer

env_file = '.env.'
if 'ENV' in os.environ:
    env_file += str(os.environ['ENV'])
else:
    env_file += 'local'
env_path = os.path.join(os.getcwd(), env_file)
load_dotenv(dotenv_path=env_path)
SERVICE_NAME = os.getenv('SERVICE_NAME')
RECORD_PATH = '/record/'


@pytest.mark.parametrize('has_env', [
    (True),
    (False),
])
def test_import_env(mocker, has_env):
    if has_env:
        os.environ['ENV'] = 'test'
    else:
        del os.environ['ENV']
    kafka_bootstrap_servers = 'http://bootstrap_servers'
    kafka_main_topic = 'topic_test'
    service_name = 'test'
    service_url = 'http://test'
    mocker.patch('dotenv.load_dotenv')
    import_env()


@pytest.mark.parametrize('method', [
    ('get'),
    ('post'),
    ('put'),
    ('delete'),
])
def test_request(mocker, method):
    mock_get = mocker.patch('requests.get')
    mock_post = mocker.patch('requests.post')
    mock_put = mocker.patch('requests.put')
    mock_delete = mocker.patch('requests.delete')
    url = 'http://test'
    topic = 'test_topic'
    consumer.request_all = {
        'get': mock_get,
        'post': mock_post,
        'put': mock_put,
        'delete': mock_delete
    }
    response = request(method, url, topic, None)
    if method == 'get':
        mock_get.assert_called_once_with(url, params=topic)
        mock_post.assert_not_called()
        mock_put.assert_not_called()
        mock_delete.assert_not_called()
    elif method == 'post':
        mock_get.assert_not_called()
        mock_post.assert_called_once_with(url, params=topic, data=None)
        mock_put.assert_not_called()
        mock_delete.assert_not_called()
    elif method == 'put':
        mock_get.assert_not_called()
        mock_post.assert_not_called()
        mock_put.assert_called_once_with(url, params=topic, data=None)
        mock_delete.assert_not_called()
    elif method == 'delete':
        mock_get.assert_not_called()
        mock_post.assert_not_called()
        mock_put.assert_not_called()
        mock_delete.assert_called_once_with(url, params=topic, data=None)


@pytest.mark.parametrize('process, pk, expect', [
    (
        'show_record',
        None,
        {'method': 'get', 'path': RECORD_PATH}
    ),
    (
        'show_record',
        '1',
        {'method': 'get', 'path': RECORD_PATH + '1/'}
    ),
    (
        'create_record',
        None,
        {'method': 'post', 'path': RECORD_PATH}
    ),
    (
        'create_record',
        '1',
        {'method': 'post', 'path': RECORD_PATH + '1/'}
    ),
    (
        'update_record',
        None,
        {'method': 'put', 'path': RECORD_PATH}
    ),
    (
        'update_record',
        '1',
        {'method': 'put', 'path': RECORD_PATH + '1/'}
    ),
])
def test_get_process(mocker, process, pk, expect):
    do = get_process(process, pk)
    url_parse = urlparse(do['url'])
    assert do['method'] == expect['method']
    assert url_parse.path == expect['path']


def test_init(mocker):
    mocker.patch.object(consumer, '__name__', '__main__')
    mocker.patch('consumer.get_process')
    mocker.patch('consumer.request', return_value=(
        AttrDict({
            'status_code': 200,
            'text': 'OK'
        })
    ))
    mocker.patch('kafka.KafkaConsumer', return_value=([
        AttrDict({
            'value': {
                'service': SERVICE_NAME,
                'process': 'show_record',
                'topic': 'test_topic',
                'pk': '1'
            }
        })
    ]))
    consumer.init()


def test_heartbeat():
    factory = RequestFactory()
    request = factory.get('/heart-beat')
    response = heartbeat(request)
    assert response.status_code == 200
    assert response.data == 'OK'