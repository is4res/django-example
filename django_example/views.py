from rest_framework.response import Response
from rest_framework.decorators import api_view, authentication_classes, \
    permission_classes


@api_view(["GET"])
@authentication_classes(())
@permission_classes(())
def heartbeat(request):
    return Response("OK")
