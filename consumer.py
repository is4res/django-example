import os
import json
import requests
import kafka
import dotenv
from time import sleep


def import_env():
    # import env
    env_file = '.env.'
    if 'ENV' in os.environ:
        env_file += str(os.environ['ENV'])
    else:
        env_file += 'local'
    env_path = os.path.join(os.getcwd(), env_file)
    dotenv.load_dotenv(dotenv_path=env_path)


# settings
import_env()
KAFKA_BOOTSTRAP_SERVERS = os.getenv('KAFKA_BOOTSTRAP_SERVERS')
KAFKA_MAIN_TOPIC = os.getenv('KAFKA_MAIN_TOPIC')
SERVICE_NAME = os.getenv('SERVICE_NAME')
SERVICE_URL = os.getenv('SERVICE_URL')
RECORD_URL = SERVICE_URL + '/record/'
request_all = {
    'get': requests.get,
    'post': requests.post,
    'put': requests.put,
    'delete': requests.delete
}


def request(method, url, topic, data):
    """
    send request
    """

    if method != 'get':
        return request_all[method](url, params=topic, data=data)
    else:
        return request_all[method](url, params=topic)


def get_process(process, pk):
    """
    Get what to do
    """

    processes = {
        'get_record': {
            'method': 'get',
            'url': RECORD_URL
        },
        'show_record': {
            'method': 'get',
            'url': RECORD_URL
        },
        'create_record': {
            'method': 'post',
            'url': RECORD_URL
        },
        'update_record': {
            'method': 'put',
            'url': RECORD_URL
        }
    }
    do = processes[process]
    if pk:
        do['url'] += pk + '/'
    return do


def init():
    if __name__ == '__main__':
        consumer = kafka.KafkaConsumer(
            KAFKA_MAIN_TOPIC,
            bootstrap_servers=[KAFKA_BOOTSTRAP_SERVERS],
            value_deserializer=lambda x: json.loads(x.decode('utf-8'))
        )
        for msg in consumer:
            data = msg.value
            if data['service'] == SERVICE_NAME:
                do = get_process(data['process'], data['pk'])
                topic = {
                    'topic': data['topic']
                }
                body = data['data'] if 'data' in data else None
                response = request(do['method'], do['url'], topic, body)

                # keep log
                log = {
                    'message': data,
                    'answer': {
                        'status_code': response.status_code,
                        'data': response.text
                    }
                }
                print(log)
                f = open('kafka.log', 'a')
                f.write(json.dumps(log) + '\n')
                f.close()


# run
init()