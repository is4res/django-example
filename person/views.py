from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from rest_framework import status, permissions, authentication

from django.core.cache import cache
from django.views.decorators.cache import cache_page
from django.conf import settings
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required

from .models import Person


@api_view(['GET'])
@permission_classes([permissions.AllowAny])
@cache_page(settings.CACHES['default']['TIMEOUT'], key_prefix='__person__')
def view_cache_page_persons(request):
    persons = Person.objects.all()
    results = [person.to_json() for person in persons]
    return Response(results, status=200)


@api_view(['GET'])
@permission_classes([permissions.AllowAny])
def view_cache_persons(request):
    try:
        if 'person' in cache:
            # get results from cache
            results = cache.get('__person__')
        else:
            results = get_person()
            cache.set('__person__', results)
    except Exception as e:
        results = get_person()

    return Response(results, status=200)


def get_person():
    persons = Person.objects.all()
    results = [person.to_json() for person in persons]
    return results


class PersonViewSet(ViewSet):
    """
    Manage person
    """

    permission_classes = [permissions.AllowAny]

    @method_decorator(permission_required('person.view'))
    @method_decorator(cache_page(
        settings.CACHES['default']['TIMEOUT'],
        key_prefix='__person__'
    ))
    def list(self, request):
        """
        Get persons
        """

        results = get_person()
        return Response(results, status=200)

    @method_decorator(permission_required('person.add'))
    def create(self, request):
        """
        Add person
        """

        return Response(status=201)

    @method_decorator(permission_required('person.view'))
    def retrieve(self, request, pk):
        """
        Show person
        """

        return Response(status=200)

    @method_decorator(permission_required('person.change'))
    def update(self, request, pk):
        """
        Update person
        """

        return Response(status=204)

    @method_decorator(permission_required('person.change'))
    def partial_update(self, request, pk):
        """
        Update person
        """

        return Response(status=204)

    @method_decorator(permission_required('person.delete'))
    def destroy(self, request, pk):
        """
        Delete person
        """

        return Response(status=204)
