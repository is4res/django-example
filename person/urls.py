from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import view_cache_page_persons, view_cache_persons, PersonViewSet

router = DefaultRouter()
router.register('', PersonViewSet, basename='person')

urlpatterns = [
    path('cache-page/', view_cache_page_persons),
    path('cache/', view_cache_persons),
]

urlpatterns += router.urls
