# Django Example
Django restful example using Django Rest framework contains Redis and Apache Kafka

## Start
- Copy .env.example to .env.local
- pytest.ini change `DJANGO_SETTINGS_MODULE`
- docker-commpose.yml change service and hostname

## Contains
- Redis
- Kafka

## Redis
- docker-compose for redis
```
version: "3.5"

services:
  redis:
    image: redis:5-alpine
    container_name: redis
    hostname: redis
    ports:
      - 6379:6379
    networks:
      - main

networks:
  main:
    external:
      name: main-network
```
- django_example.settings
```
# Cache configuration
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/",
        "TIMEOUT": 60,
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}
```
- person.views

```
# using django cache
@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
@cache_page(settings.CACHES['default']['TIMEOUT'], key_prefix='__person__')
def view_cache_page_persons(request):
    persons = Person.objects.all()
    results = [person.to_json() for person in persons]
    return Response(results, status=200)

# handle cache by manually
@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def view_cache_persons(request):
    try:
        if 'person' in cache:
            # get results from cache
            results = cache.get('__person__')
        else:
            results = get_person()
            cache.set('__person__', results)
    except Exception as e:
        results = get_person()

    return Response(results, status=200)

def get_person():
    persons = Person.objects.all()
    results = [person.to_json() for person in persons]
    return results
```

## Kafka
[getting-started-with-apache-kafka-in-python](https://towardsdatascience.com/getting-started-with-apache-kafka-in-python-604b3250aa05)

#### Commands
- list topics
```
kafka-topics.sh --list --zookeeper <zookeeper_host>
```
- sendding messages (producer)
```
kafka-console-producer.sh --broker-list <kafka_host> --topic <topic_name>
```
- list message in topic (consuming messages)
```
kafka-console-consumer.sh --bootstrap-server <kafka_host> --topic <topic_name> --from-beginning
```

## Deploy
**build**
```
docker build -t django-redis-example .
```
**run**
```
docker-compose up -d
```

## References
- [How to Cache Using Redis in Django Applications](https://code.tutsplus.com/tutorials/how-to-cache-using-redis-in-django-applications--cms-30178)
- [Django Cache](https://docs.djangoproject.com/en/2.2/topics/cache/)

## Note
- kill process with ps
```
kill -9 $(ps -ef | grep -m 1 "python manage.py runserver" | awk '{print $2}')
```
