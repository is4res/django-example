FROM alpine:3.10

# Install packages
RUN apk update && \
    apk add --update --no-cache python3 \
    python3-dev \
    gcc \
    musl-dev

# Set timezone
RUN apk add tzdata && \
    ln -snf /usr/share/zoneinfo/Asia/Bangkok /etc/localtime

# Install gunicorn webserver
RUN pip3 install -U pip && \
    pip3 install \
        gunicorn \
        pipenv

# Copy source code to docker image
COPY . /var/www/html
WORKDIR /var/www/html

# Install require packages
ARG TEST
RUN pipenv lock --python "$(python3 -V | cut -d' ' -f2)" --requirements > requirements.txt
RUN if [ "x$TEST" != "x" ]; then \
        pipenv lock --python "$(python3 -V | cut -d' ' -f2)" --dev --requirements >> requirements.txt; \
    fi;
RUN pip3 install -r requirements.txt

# Expose public port
EXPOSE 80

# Set default command
CMD ["./start.sh"]

